## 数据库相关sql语句实现：

```sql
#评委名单表
CREATE TABLE `judge` (
  `judge_id` int NOT NULL AUTO_INCREMENT,
  `judge_number` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`judge_id`)
);
```

```sql
#学生名单表
CREATE TABLE student (
  student_id INT AUTO_INCREMENT PRIMARY KEY,
  student_number VARCHAR(20),
  name VARCHAR(50),
  name_pinyin VARCHAR(50),
  identification VARCHAR(18),
  native_place VARCHAR(100),
  nationality VARCHAR(50),
  gender VARCHAR(10),
  birth_date DATE,
  politics VARCHAR(50),
  class_name VARCHAR(50),
  major VARCHAR(50),
  student_type VARCHAR(50),
  year VARCHAR(50),
  tutor_id INT,
  FOREIGN KEY (tutor_id) REFERENCES judge(judge_id)
);

```

```SQL
#学工管理名单表
CREATE TABLE admin (
  admin_id INT AUTO_INCREMENT PRIMARY KEY,
  admin_number VARCHAR(20),
  name VARCHAR(50)
);
```

```SQL
#用户表
CREATE TABLE `user` (
  `user_id` INT AUTO_INCREMENT PRIMARY KEY,
  `user_type` ENUM('学生', '评委', '学工') NOT NULL,
  `student_id` INT,
  `judge_id` INT,
  `admin_id` INT,
  `username` VARCHAR(50) NOT NULL,
  `password` VARCHAR(50) NOT NULL,
  FOREIGN KEY (`student_id`) REFERENCES `student` (`student_id`),
  FOREIGN KEY (`judge_id`) REFERENCES `judge` (`judge_id`),
  FOREIGN KEY (`admin_id`) REFERENCES `admin` (`admin_id`)
);
```


```sql
#个人总结
CREATE TABLE `personinfo` (
  `person_info_id` int NOT NULL AUTO_INCREMENT,
  `student_id` int NOT NULL,
  `moral_eval` LONGTEXT NOT NULL,
  `study_eval` LONGTEXT NOT NULL,
  `active_eval` LONGTEXT NOT NULL,
  PRIMARY KEY (`person_info_id`),
  FOREIGN KEY (student_id) REFERENCES student(student_id)
);
```

```sql
#科研情况
CREATE TABLE `research` (
  `research_id` int NOT NULL AUTO_INCREMENT,
  `student_id` int NOT NULL,
  `research_type` varchar(255) NOT NULL,
  `start_time` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `workout` varchar(255) NOT NULL,
  `workout_level` varchar(255) NOT NULL,
  `content` varchar(255) NOT NULL,
  PRIMARY KEY (`research_id`),
  FOREIGN KEY (student_id) REFERENCES student(student_id)
);
```

```sql
#学生骨干服务岗位
CREATE TABLE `work` (
  `work_id` int NOT NULL AUTO_INCREMENT,
  `student_id` int NOT NULL,
  `start_time` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `position` varchar(255) NOT NULL,
  `position_level` varchar(255) NOT NULL,
  `honor` varchar(255) NOT NULL,
  PRIMARY KEY (`work_id`),
  FOREIGN KEY (student_id) REFERENCES student(student_id)
);
```

```sql
#社会实践
CREATE TABLE `practice` (
  `practice_id` int NOT NULL AUTO_INCREMENT,
  `student_id` int NOT NULL,
  `start_time` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `practice_name` varchar(255) NOT NULL,
  `honor` varchar(255) NOT NULL,
  `honor_level` varchar(255) NOT NULL,
  `content` varchar(255) NOT NULL,
  PRIMARY KEY (`practice_id`),
  FOREIGN KEY (student_id) REFERENCES student(student_id)
);
```

```SQL
#学生成绩
CREATE TABLE grade (
  grade_id INT AUTO_INCREMENT PRIMARY KEY,
  student_id INT,
  average_grade DECIMAL(5, 2),
  FOREIGN KEY (student_id) REFERENCES student(student_id)
);
```

```SQL
#志愿服务
CREATE TABLE volunteer (
  volunteer_id INT AUTO_INCREMENT PRIMARY KEY,
  student_id INT,
  volunteer_time DECIMAL(5, 2),
  FOREIGN KEY (student_id) REFERENCES student(student_id)
);
```

```SQL
#总评分表
CREATE TABLE totalscore (
  score_id INT AUTO_INCREMENT PRIMARY KEY,
  student_id INT, 
  student_number VARCHAR(20),
  name VARCHAR(50),
  name_pinyin VARCHAR(50),
  identification VARCHAR(18),
  native_place VARCHAR(100),
  nationality VARCHAR(50),
  gender VARCHAR(10),
  birth_date DATE,
  politics VARCHAR(50),
  class_name VARCHAR(50),
  major VARCHAR(50),
  student_type VARCHAR(50),
  #evaluation_year INT,
  year VARCHAR(50),
  tutor_id INT,
  person_info_score DECIMAL(5, 2),
  research_score DECIMAL(5, 2),
  work_score DECIMAL(5, 2),
  practice_score DECIMAL(5, 2),
  #judge_score_sum DECIMAL(5, 2),
  volunteer_score DECIMAL(5, 2),
  grade_score DECIMAL(5, 2),
  total_score DECIMAL(5, 2),
  FOREIGN KEY (tutor_id) REFERENCES judge(judge_id),  
  FOREIGN KEY (student_id) REFERENCES student(student_id)  
);
```

完整版

```mysql
CREATE TABLE `judge` (
  `judge_id` int NOT NULL AUTO_INCREMENT,
  `judge_number` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`judge_id`)
);
CREATE TABLE student (
  student_id INT AUTO_INCREMENT PRIMARY KEY,
  student_number VARCHAR(20),
  name VARCHAR(50),
  name_pinyin VARCHAR(50),
  identification VARCHAR(18),
  native_place VARCHAR(100),
  nationality VARCHAR(50),
  gender VARCHAR(10),
  birth_date DATE,
  politics VARCHAR(50),
  class_name VARCHAR(50),
  major VARCHAR(50),
  student_type VARCHAR(50),
  year VARCHAR(50),
  tutor_id INT,
  FOREIGN KEY (tutor_id) REFERENCES judge(judge_id)
);
CREATE TABLE admin (
  admin_id INT AUTO_INCREMENT PRIMARY KEY,
  admin_number VARCHAR(20),
  name VARCHAR(50)
);
CREATE TABLE `personinfo` (
  `person_info_id` int NOT NULL AUTO_INCREMENT,
  `student_id` int NOT NULL,
  `moral_eval` LONGTEXT NOT NULL,
  `study_eval` LONGTEXT NOT NULL,
  `active_eval` LONGTEXT NOT NULL,
  PRIMARY KEY (`person_info_id`),
  FOREIGN KEY (student_id) REFERENCES student(student_id)
);
CREATE TABLE `research` (
  `research_id` int NOT NULL AUTO_INCREMENT,
  `student_id` int NOT NULL,
  `research_type` varchar(255) NOT NULL,
  `start_time` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `workout` varchar(255) NOT NULL,
  `workout_level` varchar(255) NOT NULL,
  `content` varchar(255) NOT NULL,
  PRIMARY KEY (`research_id`),
  FOREIGN KEY (student_id) REFERENCES student(student_id)
);
CREATE TABLE `work` (
  `work_id` int NOT NULL AUTO_INCREMENT,
  `student_id` int NOT NULL,
  `start_time` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `position` varchar(255) NOT NULL,
  `position_level` varchar(255) NOT NULL,
  `honor` varchar(255) NOT NULL,
  PRIMARY KEY (`work_id`),
  FOREIGN KEY (student_id) REFERENCES student(student_id)
);
CREATE TABLE `practice` (
  `practice_id` int NOT NULL AUTO_INCREMENT,
  `student_id` int NOT NULL,
  `start_time` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `practice_name` varchar(255) NOT NULL,
  `honor` varchar(255) NOT NULL,
  `honor_level` varchar(255) NOT NULL,
  `content` varchar(255) NOT NULL,
  PRIMARY KEY (`practice_id`),
  FOREIGN KEY (student_id) REFERENCES student(student_id)
);
CREATE TABLE grade (
  grade_id INT AUTO_INCREMENT PRIMARY KEY,
  student_id INT,
  average_grade DECIMAL(5, 2),
  FOREIGN KEY (student_id) REFERENCES student(student_id)
);
CREATE TABLE volunteer (
  volunteer_id INT AUTO_INCREMENT PRIMARY KEY,
  student_id INT,
  volunteer_time DECIMAL(5, 2),
  FOREIGN KEY (student_id) REFERENCES student(student_id)
);
CREATE TABLE totalscore (
  score_id INT AUTO_INCREMENT PRIMARY KEY,
  student_id INT, 
  student_number VARCHAR(20),
  name VARCHAR(50),
  name_pinyin VARCHAR(50),
  identification VARCHAR(18),
  native_place VARCHAR(100),
  nationality VARCHAR(50),
  gender VARCHAR(10),
  birth_date DATE,
  politics VARCHAR(50),
  class_name VARCHAR(50),
  major VARCHAR(50),
  student_type VARCHAR(50),
  #evaluation_year INT,
  year VARCHAR(50),
  tutor_id INT,
  person_info_score DECIMAL(5, 2),
  research_score DECIMAL(5, 2),
  work_score DECIMAL(5, 2),
  practice_score DECIMAL(5, 2),
  #judge_score_sum DECIMAL(5, 2),
  volunteer_score DECIMAL(5, 2),
  grade_score DECIMAL(5, 2),
  total_score DECIMAL(5, 2),
  FOREIGN KEY (tutor_id) REFERENCES judge(judge_id),  
  FOREIGN KEY (student_id) REFERENCES student(student_id)  
);
CREATE TABLE user (
  user_id INT PRIMARY KEY AUTO_INCREMENT,
  username VARCHAR(50),
  password VARCHAR(50),
  relation_id INT
);

CREATE TABLE role (
  role_id INT PRIMARY KEY AUTO_INCREMENT,
  role_name VARCHAR(50) NOT NULL
);

CREATE TABLE user_role_relation (
  relation_id INT AUTO_INCREMENT,
  user_id INT,
  role_id INT,
  PRIMARY KEY (relation_id),
  FOREIGN KEY (user_id) REFERENCES user (user_id),
  FOREIGN KEY (role_id) REFERENCES role (role_id)
);

CREATE TABLE permission (
  permission_id INT PRIMARY KEY AUTO_INCREMENT,
  permission_content VARCHAR(50) NOT NULL
);

CREATE TABLE role_permission_relation (
  relation_id INT AUTO_INCREMENT,
  role_id INT,
  permission_id INT,
  PRIMARY KEY (relation_id),
  FOREIGN KEY (role_id) REFERENCES role (role_id),
  FOREIGN KEY (permission_id) REFERENCES permission (permission_id)
);

CREATE TABLE router (
  router_id INT AUTO_INCREMENT,
  path VARCHAR(255),
  name VARCHAR(255),
  component VARCHAR(255),
  redirect VARCHAR(255),
  title VARCHAR(255),
  children INT,
  sister INT,
  hidden BOOLEAN,
  PRIMARY KEY (router_id)
);

CREATE TABLE router_permission_relation (
  relation_id INT AUTO_INCREMENT,
  router_id INT,
  permission_id INT,
  PRIMARY KEY (relation_id),
  FOREIGN KEY (router_id) REFERENCES router (router_id),
  FOREIGN KEY (permission_id) REFERENCES permission (permission_id)
);

INSERT INTO `user` (`username`, `password`, `relation_id`) VALUES ('pro202301','1111','1');
INSERT INTO `user` (`username`, `password`, `relation_id`) VALUES ('stu202301','1111','1');
INSERT INTO `user` (`username`, `password`, `relation_id`) VALUES ('pro202303','3333','3');
INSERT INTO `user` (`username`, `password`, `relation_id`) VALUES ('pro202302','2222','2');
INSERT INTO `user` (`username`, `password`, `relation_id`) VALUES ('admin202301','1111','1');
INSERT INTO `user` (`username`, `password`, `relation_id`) VALUES ('pro202306','6666','6');
INSERT INTO `user` (`username`, `password`, `relation_id`) VALUES ('pro202304','4444','4');
INSERT INTO `user` (`username`, `password`, `relation_id`) VALUES ('pro202305','5555','5');
INSERT INTO `user` (`username`, `password`, `relation_id`) VALUES ('stu202302','2222','2');
INSERT INTO `user` (`username`, `password`, `relation_id`) VALUES ('stu202303','3333','3');
INSERT INTO `user` (`username`, `password`, `relation_id`) VALUES ('stu202304','4444','4');
INSERT INTO `user` (`username`, `password`, `relation_id`) VALUES ('stu202305','5555','5');

INSERT INTO `admin` (`admin_number`, `name`) VALUES ('202201','鲁迅');

INSERT INTO `role` (`role_name`) VALUES ('学生');
INSERT INTO `role` (`role_name`) VALUES ('评委-个人总结');
INSERT INTO `role` (`role_name`) VALUES ('评委-科研情况');
INSERT INTO `role` (`role_name`) VALUES ('评委-骨干工作');
INSERT INTO `role` (`role_name`) VALUES ('评委-社会实践');
INSERT INTO `role` (`role_name`) VALUES ('评委-学生成绩');
INSERT INTO `role` (`role_name`) VALUES ('评委-志愿服务');
INSERT INTO `role` (`role_name`) VALUES ('教务');

INSERT INTO `permission` (`permission_content`) VALUES ('学生');
INSERT INTO `permission` (`permission_content`) VALUES ('评委-个人总结');
INSERT INTO `permission` (`permission_content`) VALUES ('评委-科研情况');
INSERT INTO `permission` (`permission_content`) VALUES ('评委-骨干工作');
INSERT INTO `permission` (`permission_content`) VALUES ('评委-社会实践');
INSERT INTO `permission` (`permission_content`) VALUES ('评委-学生成绩');
INSERT INTO `permission` (`permission_content`) VALUES ('评委-志愿服务');
INSERT INTO `permission` (`permission_content`) VALUES ('教务');

INSERT INTO `user_role_relation` (`user_id`, `role_id`) VALUES ('1', '2');
INSERT INTO `user_role_relation` (`user_id`, `role_id`) VALUES ('2', '1');
INSERT INTO `user_role_relation` (`user_id`, `role_id`) VALUES ('3', '4');
INSERT INTO `user_role_relation` (`user_id`, `role_id`) VALUES ('4', '3');
INSERT INTO `user_role_relation` (`user_id`, `role_id`) VALUES ('5', '8');
INSERT INTO `user_role_relation` (`user_id`, `role_id`) VALUES ('6', '7');
INSERT INTO `user_role_relation` (`user_id`, `role_id`) VALUES ('7', '5');
INSERT INTO `user_role_relation` (`user_id`, `role_id`) VALUES ('8', '6');
INSERT INTO `user_role_relation` (`user_id`, `role_id`) VALUES ('9', '1');
INSERT INTO `user_role_relation` (`user_id`, `role_id`) VALUES ('10', '1');
INSERT INTO `user_role_relation` (`user_id`, `role_id`) VALUES ('11', '1');
INSERT INTO `user_role_relation` (`user_id`, `role_id`) VALUES ('12', '1');


INSERT INTO `role_permission_relation` (`role_id`, `permission_id`) VALUES ('1', '1');
INSERT INTO `role_permission_relation` (`role_id`, `permission_id`) VALUES ('2', '2');
INSERT INTO `role_permission_relation` (`role_id`, `permission_id`) VALUES ('3', '3');
INSERT INTO `role_permission_relation` (`role_id`, `permission_id`) VALUES ('4', '4');
INSERT INTO `role_permission_relation` (`role_id`, `permission_id`) VALUES ('5', '5');
INSERT INTO `role_permission_relation` (`role_id`, `permission_id`) VALUES ('6', '6');
INSERT INTO `role_permission_relation` (`role_id`, `permission_id`) VALUES ('7', '7');
INSERT INTO `role_permission_relation` (`role_id`, `permission_id`) VALUES ('8', '8');

INSERT INTO `router` (`path`, `component`, `redirect`, `children`) VALUES ('','Layout','dashboard','2');
INSERT INTO `router` (`path`, `component`, `title`) VALUES ('dashboard','Home','首页');
INSERT INTO `router` (`path`,`component`, `redirect`, `title`, `children`) VALUES ('/stu','Layout','dashboard','学生模块','4');
INSERT INTO `router` (`path`,`name`,`component`, `title`, `sister`) VALUES ('persinalinfo','persinalinfo','stu/stu-personalinfo','个人总结','5');
INSERT INTO `router` (`path`,`name`,`component`, `title`, `sister`) VALUES ('sciResearch','sciResearch','stu/stu-sciResearch','科研情况','6');
INSERT INTO `router` (`path`,`name`,`component`, `title`,`sister`) VALUES ('xuegu','xuegu','stu/stu-xuegu','骨干工作','7');
INSERT INTO `router` (`path`,`name`,`component`, `title`,`sister`) VALUES ('shijian','shijian','stu/stu-shijian','社会实践','23');
INSERT INTO `router` (`path`,`component`, `redirect`, `title`, `children`) VALUES ('/pro','Layout','dashboard','评委模块','9');
INSERT INTO `router` (`path`,`name`,`component`, `title`) VALUES ('personalinfo','personalinfo','pro/pro-personalinfo','个人总结');
INSERT INTO `router` (`path`,`component`, `redirect`, `title`, `children`) VALUES ('/pro','Layout','dashboard','评委模块','11');
INSERT INTO `router` (`path`,`name`,`component`, `title`) VALUES ('sciResearch','sciResearch','pro/pro-sciResearch','科研情况');
INSERT INTO `router` (`path`,`component`, `redirect`, `title`, `children`) VALUES ('/pro','Layout','dashboard','评委模块','13');
INSERT INTO `router` (`path`,`name`,`component`, `title`) VALUES ('shijian','shijian','pro/pro-shijian','社会实践');
INSERT INTO `router` (`path`,`component`, `redirect`, `title`, `children`) VALUES ('/pro','Layout','dashboard','评委模块','15');
INSERT INTO `router` (`path`,`name`,`component`, `title`) VALUES ('xuegu','xuegu','pro/pro-xuegu','骨干工作');
INSERT INTO `router` (`path`,`component`, `redirect`, `title`, `children`) VALUES ('/pro','Layout','dashboard','评委模块','17');
INSERT INTO `router` (`path`,`name`,`component`, `title`) VALUES ('confirmvoluntarytime','confirmvoluntarytime','pro/pro-confirmVoluntaryTime','志愿服务');
INSERT INTO `router` (`path`,`component`, `redirect`, `title`, `children`) VALUES ('/pro','Layout','dashboard','评委模块','19');
INSERT INTO `router` (`path`,`name`,`component`, `title`) VALUES ('confirmgrade','confirmgrade','pro/pro-confirmGrade','学生成绩');
INSERT INTO `router` (`path`,`component`, `redirect`, `title`, `children`) VALUES ('/admin','Layout','dashboard','教务处模块','21');
INSERT INTO `router` (`path`,`name`,`component`, `title`, `sister`) VALUES ('studentList','studentList','admin/admin-studentList','导入学生名单','22');
INSERT INTO `router` (`path`,`name`,`component`, `title`) VALUES ('summaryGrade','summaryGrade','admin/admin-summaryGrade','学生成绩汇总');
INSERT INTO `router` (`path`,`name`,`component`, `title`) VALUES ('importFiles','importFiles','stu/stu-importFiles','上传相关文件');


INSERT INTO `router_permission_relation` (`router_id`, `permission_id`) VALUES ('1', '1');
INSERT INTO `router_permission_relation` (`router_id`, `permission_id`) VALUES ('3', '1');
INSERT INTO `router_permission_relation` (`router_id`, `permission_id`) VALUES ('1', '2');
INSERT INTO `router_permission_relation` (`router_id`, `permission_id`) VALUES ('8', '2');
INSERT INTO `router_permission_relation` (`router_id`, `permission_id`) VALUES ('1', '3');
INSERT INTO `router_permission_relation` (`router_id`, `permission_id`) VALUES ('10', '3');
INSERT INTO `router_permission_relation` (`router_id`, `permission_id`) VALUES ('1', '4');
INSERT INTO `router_permission_relation` (`router_id`, `permission_id`) VALUES ('12', '4');
INSERT INTO `router_permission_relation` (`router_id`, `permission_id`) VALUES ('1', '5');
INSERT INTO `router_permission_relation` (`router_id`, `permission_id`) VALUES ('14', '5');
INSERT INTO `router_permission_relation` (`router_id`, `permission_id`) VALUES ('1', '6');
INSERT INTO `router_permission_relation` (`router_id`, `permission_id`) VALUES ('18', '6');
INSERT INTO `router_permission_relation` (`router_id`, `permission_id`) VALUES ('1', '7');
INSERT INTO `router_permission_relation` (`router_id`, `permission_id`) VALUES ('16', '7');
INSERT INTO `router_permission_relation` (`router_id`, `permission_id`) VALUES ('1', '8');
INSERT INTO `router_permission_relation` (`router_id`, `permission_id`) VALUES ('20', '8');


#新建存储学生上传证明材料的文件名（重命名过的文件名）
CREATE TABLE `files_imported_by_stu` (
  `file_id` int NOT NULL AUTO_INCREMENT,
  `student_id` int NOT NULL,
  `module_name` varchar(255) NOT NULL,#每个模块只能上传一个文件
  `file_unique_name` varchar(255) NOT NULL,
  `delete_sign` tinyInt NOT NULL,#标识该资料是否被删除(1:删除;0:未删除)，即使被删除，服务器端也不会真删除
  PRIMARY KEY (`file_id`),
  FOREIGN KEY (student_id) REFERENCES student(student_id)
);
```





