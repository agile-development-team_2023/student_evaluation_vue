import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import Antd from 'ant-design-vue/es';
import 'ant-design-vue/dist/antd.css';

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import { Message } from "element-ui";

import axios from 'axios';
axios.defaults.withCredentials = true
axios.defaults.baseURL = '/api'
Vue.use(Antd);

import './permission'
import './utils/global'


// Vue.use(Message);
// Vue.prototype.$message = Message;

Vue.use(ElementUI);

Vue.use(Antd);

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
