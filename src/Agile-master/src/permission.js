import router from './router'

import { getAccessToken } from '@/utils/auth'
import { getRBAC } from '@/api/rbac'
const _import = require('./router/_import_' + process.env.NODE_ENV)
import Layout from './layout' //Layout 是架构组件，不在后台返回，在文件里单独引入
var getRouter //用来获取后台拿到的路由

router.beforeEach((to, from, next) => {
  if (getAccessToken()) {
    if (!getRouter) { //不加这个判断，路由会陷入死循环
      if (!getObjArr('router')) {
  
        // axios.get('http://localhost:28080/my-api/rbac/rbac').then(res => {
        // console.log('beforeEach  getRouter')
        // getRouter = fakeRouter.router //假装模拟后台请求得到的路由数据
        // saveObjArr('router', getRouter) //存储路由到localStorage
        getRBAC().then(res => {
          getRouter = res.data
          console.log(getRouter)
          routerGo(to, next) //执行路由跳转方法
        })
      } else { //从localStorage拿到了路由
        getRouter = getObjArr('router') //拿到路由
        console.log(getRouter)
        routerGo(to, next)
      }
    } else {
      next()
    }
    if (to.path === '/login') {
      next({ path: '' })
    } 
    next()
  } else {
    // 没有token
    if (to.path === '/login') {
      // 直接进入
      next()
    } 
    else{
      next('/login') 
    }
    // 否则全部重定向到登录页
  }
})

function routerGo(to, next) {
  getRouter = filterAsyncRouter(getRouter) //过滤路由
  for (let route of getRouter) {
    router.addRoute(route); // 动态添加路由
  }
  global.antRouter = getRouter //将路由数据传递给全局变量，做侧边栏菜单渲染工作
  next({ ...to, replace: true })
}

function saveObjArr(name, data) { //localStorage 存储数组对象的方法
  localStorage.setItem(name, JSON.stringify(data))
}

function getObjArr(name) { //localStorage 获取数组对象的方法
  return JSON.parse(window.localStorage.getItem(name));

}

function filterAsyncRouter(asyncRouterMap) { //遍历后台传来的路由字符串，转换为组件对象
  const accessedRouters = asyncRouterMap.filter(route => {
    if (route.component) {
      if (route.component === 'Layout') { //Layout组件特殊处理
        route.component = Layout;
      } else {
        route.component = _import(route.component)
      }
    }
    if (route.children && route.children.length) {
      route.children = filterAsyncRouter(route.children)
    }
    return true
  })

  return accessedRouters
}


router.afterEach(() => {
  
})