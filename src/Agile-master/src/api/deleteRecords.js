import request from '@/utils/requestStu'

// 1发送个人总结删除对应记录
export function sendPersonInfoId( person_info_id, token, urlConponent ) {
  const data = {
    person_info_id,
    token
  }
  return request({
    url: '/stu-api/'+urlConponent+'/delete',
    method: 'post',
    data: data //后端返回的信息
  })
}

// 2发送科研ID删除对应记录
export function sendResearchId( research_id, token, urlConponent ) {
  const data = {
    research_id, token
  }
  return request({
    url: '/stu-api/'+urlConponent+'/delete',
    method: 'post',
    data: data //后端返回的信息
  })
}

// 3发送骨干工作删除对应记录
export function sendWorkId( work_id, token, urlConponent ) {
  const data = {
    work_id, token
  }
  return request({
    url: '/stu-api/'+urlConponent+'/delete',
    method: 'post',
    data: data //后端返回的信息
  })
}

// 4发送社会实践删除对应记录
export function sendPracticeId( practice_id, token, urlConponent ) {
  const data = {
    practice_id, token
  }
  return request({
    url: '/stu-api/'+urlConponent+'/delete',
    method: 'post',
    data: data //后端返回的信息
  })
}

