import request from '@/utils/requestPro'

export function subPersonalInfo(student_id, person_info_score) {
    const data = {
        student_id,
        person_info_score,
    }
    return request({
      url: '/my-api/pro/personalInfoSub',
      method: 'post',
      data: data
    })
  }

  export function subResearch(student_id, research_score) {
    const data = {
        student_id,
        research_score,
    }
    return request({
      url: '/my-api/pro/researchSub',
      method: 'post',
      data: data
    })
  }
  export function subPractice(student_id, practice_score) {
    const data = {
        student_id,
        practice_score,
    }
    return request({
      url: '/my-api/pro/practiceSub',
      method: 'post',
      data: data
    })
  }
  export function subWork(student_id, work_score) {
    const data = {
        student_id,
        work_score,
    }
    return request({
      url: '/my-api/pro/workSub',
      method: 'post',
      data: data
    })
  }
  export function subGrade(student_id, grade_score) {
    const data = {
        student_id,
        grade_score,
    }
    return request({
      url: '/my-api/pro/gradeSub',
      method: 'post',
      data: data
    })
  }
  export function subVolunteer(student_id, volunteer_score) {
    const data = {
        student_id,
        volunteer_score,
    }
    return request({
      url: '/my-api/pro/volunteerSub',
      method: 'post',
      data: data
    })
  }