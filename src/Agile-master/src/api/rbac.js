import request from '@/utils/request'

export function getRBAC() {
    return request({
      url: 'my-api/rbac/rbac',
      method: 'get'
    })
  }