import request from '@/utils/requestPro'

export function getPersonalInfo(student_id) {
    const data = {
        student_id,
    }
    return request({
      url: '/my-api/pro/personalInfo',
      method: 'post',
      data: data
    })
  }


  export function getResearch(student_id) {
    const data = {
      student_id,
    }
    return request({
      url: '/my-api/pro/research',
      method: 'post',
      data: data
    })
  }

  export function getWork(student_id) {
    const data = {
      student_id,
    }
    return request({
      url: '/my-api/pro/work',
      method: 'post',
      data: data
    })
  }

  export function getPractice(student_id) {
    const data = {
      student_id,
    }
    return request({
      url: '/my-api/pro/practice',
      method: 'post',
      data: data
    })
  }

  export function getVolunteer(student_id) {
    const data = {
      student_id,
    }
    return request({
      url: '/my-api/pro/volunteer',
      method: 'post',
      data: data
    })
  }

  export function getGrade(student_id) {
    const data = {
      student_id,
    }
    return request({
      url: '/my-api/pro/grade',
      method: 'post',
      data: data
    })
  }

  export function getTotalScore(student_id) {
    const data = {
      student_id,
    }
    return request({
      url: '/my-api/pro/totalscore',
      method: 'post',
      data: data
    })
  }


