import request from '@/utils/requestStu'

// 发送token至后端，从中取得学生ID获取对应历史记录
export function sendStudentId( token, urlConponent ) {
  const data = {
    token
  }
  return request({
    url: '/stu-api/'+urlConponent+'/findAll',
    method: 'post',
    data: data //后端返回的信息
  })
}
