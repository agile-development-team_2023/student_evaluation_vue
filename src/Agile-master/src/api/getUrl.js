import request from '@/utils/requestPro'

export function getUrl(student_id, module_name) {
    const data = {
        student_id,
        module_name
    }
    return request({
      url: '/my-api/pro/url',
      method: 'post',
      data: data
    })
  }