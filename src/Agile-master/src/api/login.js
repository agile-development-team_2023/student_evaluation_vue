import request from '@/utils/request'
// 用户登录
export function login(username, password,captchaText) {
  const data = {
    username,
    password,
    captchaText
  }
  return request({
    url: '/auth/login',//与utils/request.js中的baseurl相拼接
    // url: '/login2',//与utils/request.js中的baseurl相拼接
    method: 'post',
    data: data
  })
}

// 获取用户信息
export function getInfo() {
  return request({
    url: 'auth/profile/get',
    method: 'get'
  })
}