import request from '@/utils/requestPro'

export function getPersonalInfoCount() {
    return request({
      url: '/my-api/pro/personalInfoCount',
      method: 'get',
    })
  }

  export function getResearchCount() {
    return request({
      url: '/my-api/pro/researchCount',
      method: 'get',
    })
  }

  export function getPracticeCount() {
    return request({
      url: '/my-api/pro/practiceCount',
      method: 'get',
    })
  }

  export function getWorkCount() {
    return request({
      url: '/my-api/pro/workCount',
      method: 'get',
    })
  }
  export function getGradeCount() {
    return request({
      url: '/my-api/pro/gradeCount',
      method: 'get',
    })
  }
  export function getVolunteerCount() {
    return request({
      url: '/my-api/pro/volunteerCount',
      method: 'get',
    })
  }