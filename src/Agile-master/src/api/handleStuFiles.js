import request from '@/utils/requestStuFiles'

// // 学生填报5:相关资料
// export function inputFile( formData ) {
//     const data = {
//         formData
//     };
//     return request({
//       url: '/stu-api/importFiles/upload',//与utils/request.js中的baseurl相拼接
//       method: 'post',
//       data: data //后端返回的信息
//     })
//   }
// 5发送文件删除对应记录
export function sendFileId( file_id, token, urlConponent ) {
    const data = {
      file_id,
      token
    }
    return request({
      url: '/stu-api/'+urlConponent+'/delete',
      method: 'post',
      data: data //后端返回的信息
    })
  }