import request from '@/utils/requestStu'

// 学生填报1:个人总结
export function updatePersonInfo( person_info_id, token , moral_eval,study_eval, active_eval ) {
  const data = {
    person_info_id,
    token,
    moral_eval,
    study_eval,
    active_eval
  }
  return request({
    url: '/stu-api/personInfo/update',//与utils/request.js中的baseurl相拼接
    method: 'post',
    data: data //后端返回的信息
  })
}

// 学生更新2:科研情况
export function updateResearch(research_id, token , research_type,start_end_time, project_name,workout,workout_level, content) {
  const data = {   
    research_id,
    token,
    research_type,
    start_end_time,
    project_name,
    workout,
    workout_level,
    content
  };
  return request({
    url: '/stu-api/research/update',//与utils/request.js中的baseurl相拼接
    method: 'post',
    data: data //后端返回的信息
  })
}

// 学生更新3:骨干工作
export function updateWork( work_id, token , start_end_time, position,position_level, honor ) {
  const data = {
    work_id,
    token,
    start_end_time,
    position,
    position_level,
    honor
  };
 
  return request({
    url: '/stu-api/work/update',//与utils/request.js中的baseurl相拼接
    method: 'post',
    data: data //后端返回的信息
  })
}

// 学生填报4:社会实践
export function updatePractice( practice_id, token , start_end_time, practice_name,honor,honor_level, content ) {
  const data = {
    practice_id,
    token,
    start_end_time,
    practice_name,
    honor,
    honor_level,
    content
  };
  return request({
    url: '/stu-api/practice/update',//与utils/request.js中的baseurl相拼接
    method: 'post',
    data: data //后端返回的信息
  })
}