import request from '@/utils/requestStu'

// 学生填报1:个人总结
export function inputPersonInfo( person_info_id, token , moral_eval,study_eval, active_eval ) {
  const data = {
    person_info_id,
    token,
    moral_eval,
    study_eval,
    active_eval
  }
  return request({
    url: '/stu-api/personInfo/input',//与utils/request.js中的baseurl相拼接
    method: 'post',
    data: data //后端返回的信息
  })
}

// 学生填报2:科研情况
export function inputResearch(research_id, token , research_type,start_end_time, project_name,workout,workout_level, content) {
  const data = {   
    research_id,
    token,
    research_type,
    start_end_time,
    project_name,
    workout,
    workout_level,
    content
  };
  return request({
    url: '/stu-api/research/input',//与utils/request.js中的baseurl相拼接
    method: 'post',
    data: data //后端返回的信息
  })
}

// 学生填报3:骨干工作
export function inputWork( work_id, token , start_end_time, position,position_level, honor ) {
  const data = {
    work_id,
    token,
    start_end_time,
    position,
    position_level,
    honor
  };
 
  return request({
    url: '/stu-api/work/input',//与utils/request.js中的baseurl相拼接
    method: 'post',
    data: data //后端返回的信息
  })
}

// 学生填报4:社会实践
export function inputPractice( practice_id, token , start_end_time, practice_name,honor,honor_level, content ) {
  const data = {
    practice_id,
    token,
    start_end_time,
    practice_name,
    honor,
    honor_level,
    content
  };
  return request({
    url: '/stu-api/practice/input',//与utils/request.js中的baseurl相拼接
    method: 'post',
    data: data //后端返回的信息
  })
}