import Vue from 'vue'
import Router from 'vue-router'
import Layout from '@/layout'

Vue.use(Router)

const originalPush = Router.prototype.push
// 修改原型对象中的push方法
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}
var constantRouterMap=[
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/user/LoginView.vue')
  }
]

// const routes = [
//   {
//     path: '/main',
//     component: Layout,
//     children: [
//       {
//         path: '',
//         component: () => import('../views/Home.vue'),
//       },
//       {
//       path: 'stu/personalinfo',
//       component: () => import('../views/stu/stu-personalinfo')
//     },
//     {
//       path: 'pro/personalinfo',
//       component: () => import('../views/pro/pro-personalinfo')
//     },
//     {
//       path: 'stu/sciresearch',
//       component: () => import('../views/stu/stu-sciResearch.vue')
//     },
//     {
//       path: 'pro/sciresearch',
//       component: () => import('../views/pro/pro-sciResearch.vue')
//     },
//     {
//       path: 'pro/shijian',
//       component: () => import('../views/pro/pro-shijian.vue')
//     },
//     {
//       path: 'pro/xuegu',
//       component: () => import('../views/pro/pro-xuegu.vue')
//     },
//     {
//       path: 'stu/xuegu',
//       component: () => import('../views/stu/stu-xuegu.vue')
//     },
//     {
//       path: 'stu/shijian',
//       component: () => import('../views/stu/stu-shijian.vue')
//     },
    
//     {
//       path: 'pro/confirmvoluntarytime',
//       component: () => import('../views/pro/pro-confirmVoluntaryTime.vue')
//     },
//     {
//       path: 'pro/confirmgrade',
//       component: () => import('../views/pro/pro-confirmGrade.vue')
//     },
//     {
//       path: 'admin/studentList',
//       component: () => import('../views/admin/admin-studentList.vue')
//     },
//     {
//       path: 'admin/summaryGrade',
//       component: () => import('../views/admin/admin-summaryGrade.vue')
//     },
//     ]
//   }
// ]

// const router = new Router({
//   mode: 'history',
//   base: process.env.BASE_URL,
//   routes
// })


export default new Router({
  routes: constantRouterMap
 
})
