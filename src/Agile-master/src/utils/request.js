import router from '@/router/index'
import axios from 'axios';
import {getAccessToken} from '@/utils/auth'
axios.defaults.withCredentials = true
axios.defaults.baseURL = '/api'


const service = axios.create({
      // 可以在这里设置特定的配置选项，如请求头、超时时间等
      headers: {
        'Content-Type': 'multipart/form-data',
      },
  // baseURL: 'https://mock.apifox.cn/m1/2428381-0-default/admin-api',//公共域名
  baseURL:'http://localhost:28080',
  // baseURL:'http://101.132.244.19:28080/admin-api',
  // baseURL:'/admin-api/',
  timeout: 5000//超时时间
});

//函数的两个参数分别是箭头函数
service.interceptors.request.use(
  config => {
    // 在请求发送之前对请求数据进行处理
    // ...
    if (getAccessToken() ) {
      config.headers['Authorization'] = 'Bearer ' + getAccessToken() // 让每个请求携带自定义token 请根据实际情况自行修改
    }

    return config;
  },
  error => {
    // 对请求错误做些什么
    console.log(error);
    return Promise.reject(error);
  }
);

service.interceptors.response.use(
  response => {
    // 对响应数据进行处理（学生填报端会将过期错误以response而非error的形式返回）
    console.log(response);

    const regex = /JWT expired at (\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z)/;
    const match = response.data.msg.match(regex);

    console.log("match:"+match)
    if (match && match[1]) {
      // 进行 JWT Token 过期处理逻辑
      alert('操作失败！账号已过期，请重新登录!');
      // 进行其他处理，例如清除本地存储、跳转到登录页等
      // 一次性清空local storage的全部内容
      localStorage.clear();
      // 跳转至登陆页面并刷新
      router.push('/login');
      location.reload()
      // 返回一个被拒绝的 Promise，以阻止消息的传递
      return Promise.reject(null);
    } else {
      return response.data;
    }

  },
  error => {
    // 对响应错误做些什么
    console.log(error);
    
    if (error.response.data.message.includes('JWT expired')) {
      // 进行 JWT Token 过期处理逻辑
      alert('操作失败！账号已过期，请重新登录!');
      // 进行其他处理，例如清除本地存储、跳转到登录页等
      // 一次性清空local storage的全部内容
      localStorage.clear();
      // 跳转至登陆页面并刷新
      router.push('/login');
      location.reload()
      // 返回一个被拒绝的 Promise，以阻止消息的传递
      return Promise.reject(null);
    } else {
      // 将错误信息返回给对应子组件的原本处理方法
      return Promise.reject(error);
    }
  }
);


export default service;