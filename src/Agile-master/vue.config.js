const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true
})
// 跨域配置
//代理的地址为后端端口号，我这边设置的是28080，port:8081是前端运行的端口号
module.exports = {
  devServer: {                //记住，别写错了devServer//设置本地默认端口  选填
      host: 'localhost',
      port: 8082,
      proxy: {                 //设置代理，必须填
          '/api': {              //设置拦截器  拦截器格式   斜杠+拦截器名字，名字可以自己定
              target: 'http://localhost:28080',     //代理的目标地址
              changeOrigin: true,              //是否设置同源，输入是的
              pathRewrite: {                   //路径重写
                  '/api': ''                     //选择忽略拦截器里面的单词
              }
          }
      }
  }
}