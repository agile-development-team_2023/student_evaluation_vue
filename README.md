# 学生综合测评系统（VUE）

1.源代码说明

小组git仓库地址：

[敏捷开发小组2023 - Gitee.com](https://gitee.com/organizations/agile-development-team_2023/projects)

含两个仓库（前端+后端）：

前端仓库地址

[学生综合测评系统（VUE）: 2023软微敏捷开发小组项目（前端） (gitee.com)](https://gitee.com/agile-development-team_2023/student_evaluation_vue)

后端仓库地址

[学生综合测评系统（SpringBoot）: 2023软微敏捷开发小组项目（后端） (gitee.com)](https://gitee.com/agile-development-team_2023/student_evaluation_springboot)



2.前端配置说明


```shell
cd src/Agile-master
npm install
npm run serve
```

3.学生综合测评系统的sql脚本与测试文件demo详见 **部署说明** 文件夹；安装教程/使用说明详见课堂派最终提交的项目文档（个人作业）

